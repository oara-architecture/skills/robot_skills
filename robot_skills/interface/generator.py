from typing import List, Optional
from abc import ABC
import logging
import os
import stat

import robot_lang

from ..common.generator import Generator
from ..common.templates import action_name, GeneratorTemplates

class InterfaceGenerator(Generator):

    def __init__(self, model: robot_lang.Model, types: robot_lang.TypeBlock):
        Generator.__init__(self, model, 'interface')
        self.__types = types

    def generate(self, prefix, author, email, license, version):
        # Create folders
        self._logger.debug("Generating package structure")
        self._generate_structure(
            dirs=[self.name, 'launch', 'extra'],
            files=[os.path.join(self.name, '__init__.py')],
            prefix=prefix)

        # Convenient .gitignore
        self._generate_file(".gitignore", "gitignore")

        deps = [f"{self.model.name}_managers"]

        # Generate package.xml
        self._generate_file("package.xml", "package.xml",
                            pkgname=self.name, 
                            version=version, 
                            deps=deps,
                            author=author,
                            email=email,
                            license=license,
                            message=False,
                            cpp=False)

        # Generate CMakeLists.txt
        self._generate_file("CMakeLists.txt", "CMakeLists.txt",
                            pkgname=self.name,
                            deps=deps,
                            setup=True,
                            resources=[],
                            data=[],
                            skills=[])

        # Generate setup.py
        self._generate_file("setup.py", "setup.py.j2",
                            pkgname=self.name,
                            version=version)

        # Data interface
        self._logger.info("Generating interface to data")
        self._generate_file(os.path.join(self.name, "dataset_interface.py"), 
                            "dataset_interface.py.j2", 
                            data=self.model.data)
        self._generate_file(os.path.join(self.name, "data_interface.py"), 
                            "data_interface.py.j2",
                            skillset=self.model.name,
                            data=self.model.data)
        
        # Resource interface
        self._logger.info("Generating interface to resources")
        self._generate_file(os.path.join(self.name, "resourceset_interface.py"), 
                                         "resourceset_interface.py.j2",
                                         resources=self.model.resources)
        self._generate_file(os.path.join(self.name, "resource_interface.py"), "resource_interface.py.j2",
                            skillset=self.model.name,
                            resources=self.model.resources)

        # Skill interface        
        self._logger.info("Generating interface to skills")
        self._generate_file(os.path.join(self.name, "logic.py"), "logic.py.j2")
        self._generate_file(os.path.join(self.name, "skillsset_interface.py"), 
                                         "skillsset_interface.py.j2",
                                         skills=self.model.skills)
        self._generate_file(os.path.join(self.name, "skill_interface.py"), "skill_interface.py.j2",
                            skillset=self.model.name,
                            skills=self.model.skills)

        # SkillSet interface
        self._logger.info("Generating interface to skillset")
        self._generate_file(os.path.join(self.name, "skillset.py"), "skillset_interface.py.j2",
                            skillset=self.model)
        self._generate_file(os.path.join(self.name, "__init__.py"), "init_interface.py.j2",
                            skillset=self.model.name)

        # Transitions interface
        self._logger.info("Generating 'transitions' interface")
        self._generate_file(os.path.join(self.name, "transitions.py"), "interface_transitions.py.j2",
                            skillset=self.model,
                            skills=self.model.skills,
                            resources=self.model.resources)

        # Extra Model file
        self._logger.info("Generating extra model file")
        self._generate_file(os.path.join('extra', f'{self.model}.rl'), "model.rl.j2", 
                            model=self.model.pretty(),
                            types=self.__types)
        for resource in self.model.resources:
            robot_lang.ResourceDot(resource).write(os.path.join(self.folder, 'extra', f'{resource}_resource.dot'))
        for skill in self.model.skills:
            robot_lang.SkillDot(skill).write(os.path.join(self.folder, 'extra', f'{skill}_skill.dot'))            


class Ros1InterfaceGenerator(InterfaceGenerator):

    def __init__(self, model: robot_lang.Model, types: robot_lang.TypeBlock):
        InterfaceGenerator.__init__(self, model, types)
        self._templates = GeneratorTemplates(['ros'])


class Ros2InterfaceGenerator(InterfaceGenerator):

    def __init__(self, model: robot_lang.Model, types: robot_lang.TypeBlock):
        InterfaceGenerator.__init__(self, model, types)
        self._templates = GeneratorTemplates(['ros2'])

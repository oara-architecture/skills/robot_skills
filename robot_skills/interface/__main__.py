import sys
import argparse
import json
import yaml
import traceback

import robot_lang

from ..common.model import get_model
from ..common.logger import logging, setup_from_arg
from .generator import Ros1InterfaceGenerator, Ros2InterfaceGenerator
from .bridge import BridgeGenerator

log = logging.getLogger('robot_skills.interface')


def main():
    parser = argparse.ArgumentParser(
        prog='python3 -m robot_skills.interface', description='Generate ROS interface')
    parser.add_argument('model', type=str, nargs='+', help='model files')
    parser.add_argument('-s', '--skillset', 
        help='Skillset name. Must be specified if the input model contains several skillsets.')
    parser.add_argument('-l', '--log', 
        help='Log level (default: info)', default='info')
    parser.add_argument('--prefix', 
        help='Prefix of the generation path folder. Typically, the path to a workspace/src.')

    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument("-1", "--ros1", dest="ros2", action='store_false',
                       help="generate managers for ROS1")
    group.add_argument("-2", "--ros2", dest="ros2", action='store_true',
                       help="generate managers for ROS2")
    group.add_argument("-b", "--bridge", action='store_true',
                       help="generate bridges for ROS1/ROS2 interoperability")

    parser.add_argument('--author', type=str, default='robot_skills',
                        help='Specify package author')
    parser.add_argument('--email', type=str, default='robot_skills@oara.onera.fr',
                        help='Specify author email')
    parser.add_argument('--license', type=str, default='BSD License',
                        help='Specify package license')
    parser.add_argument('--version', type=str, default='1.0.0',
                        help='Specify package version (in format: Major.minor.Release)')
    args = parser.parse_args()
    setup_from_arg(args.log)

    try:
        model = robot_lang.parse_all(args.model)
    except FileNotFoundError:
        log.error("Model file %s not found", args.model)
        sys.exit(1)

    try:
        skillset = get_model(model, args.skillset)

        if args.bridge:
            gen = BridgeGenerator(skillset)
        elif args.ros2:
            gen = Ros2InterfaceGenerator(skillset, model.types)
        else:
            gen = Ros1InterfaceGenerator(skillset, model.types)

        gen.generate(prefix=args.prefix, 
                     author=args.author,
                     email=args.email,
                     license=args.license,
                     version=args.version)

    except StopIteration:
        log.error("The model does not contain any skillset: nothing is generated")
        sys.exit(1)

    except Exception:
        log.error("Cannot generate interface from the model")
        traceback.print_exc()
        sys.exit(1)


if __name__ == '__main__':
    main()

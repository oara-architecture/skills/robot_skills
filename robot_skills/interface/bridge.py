from typing import List, Optional
from abc import ABC
import logging
import os
import stat

import robot_lang

from ..common.generator import Generator
from ..common.templates import action_name, GeneratorTemplates

class BridgeGenerator(Generator):

    def __init__(self, model: robot_lang.Model):
        Generator.__init__(self, model, 'bridges')
        self._templates = GeneratorTemplates(['bridge', 'ros2'])
        self._templates.add_brige_filters()

    def generate(self, prefix, author, email, license, version):
        # Create folders
        self._logger.debug("Generating package structure")
        self._generate_structure(
            dirs=['src', 'cmake', 'launch'],
            prefix=prefix)

        # Convenient .gitignore
        self._generate_file(".gitignore", "gitignore")

        deps = [f"{self.model.name}_managers"]

        # Generate package.xml
        self._generate_file("package.xml", "package.xml",
                            pkgname=self.name, 
                            version=version, 
                            deps=deps,
                            author=author,
                            email=email,
                            license=license,
                            message=False,
                            cpp=True)

        # Generate CMakeLists.txt
        self._generate_file("CMakeLists.txt", "CMakeLists.txt",
                            pkgname=self.name,
                            deps=deps,
                            resources=self.model.resources,
                            data=self.model.data,
                            skills=self.model.skills)

        # Generate cmake helpers
        self._generate_file(os.path.join("cmake", "find_ros1_package.cmake"),
                            "find_ros1_package.cmake")

        # Generate Resource Bridge
        if self.model.resources:
            self._logger.debug("Generating resource bridges")
            self._generate_file(os.path.join('src', "resource_bridge.cpp"),
                                "resource_bridge.cpp.j2",
                                pkgname=f"{self.model}_managers",
                                resources=self.model.resources)

        # Generate Data Bridges
        if self.model.data:
            self._logger.debug("Generating data bridges")
            self._generate_file(os.path.join('src', "data_bridge.cpp"),
                            "data_bridge.cpp.j2",
                            pkgname=f"{self.model}_managers",
                            data=self.model.data)
        for data in self.model.data:
            self._generate_protected_file(os.path.join('src', f"{data}_convert.hpp"),
                                          "data_convert.hpp.j2",
                                          pkgname=f"{self.model}_managers",
                                          data=data)

        # Generate Skill Bridges
        self._logger.debug("Generating skill action bridges")
        for skill in self.model.skills:
            self._generate_file(os.path.join('src', f"{skill}_bridge.cpp"),
                              "action_bridge.cpp.j2",
                              pkgname=f"{self.model}_managers",
                              skill=skill)
            self._generate_protected_file(os.path.join('src', f"{skill}_convert.hpp"),
                                "action_convert.hpp.j2",
                                pkgname=f"{self.model}_managers",
                                skill=skill)

        # Generate launch
        self._generate_file(os.path.join('launch', f"{self.model}_bridges.launch.py"),
                      "bridges.launch.py.j2",
                      pkgname=self.name, 
                      skills=self.model.skills,
                      data=self.model.data,
                      resources=self.model.resources)

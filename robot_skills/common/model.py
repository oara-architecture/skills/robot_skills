import logging

log = logging.getLogger(__name__)

def get_model(model, skillset):
    if skillset:
        try:
            return next(m for m in model.skillsets if m.name == skillset)
        except StopIteration as ex:
            log.error("Skillset '%s' not defined in model", skillset)
            log.debug("The model contains skillsets %s",
                      [s.name for s in model.skillsets])
            raise(ex)

    elif len(model.skillsets) > 1:
        log.error("""Model contains more than one skillsets. 
            You must specify which robot to use with the --skillset option.""")
        log.debug("The model contains skillsets %s",
                  [s.name for s in model.skillsets])
        raise(Exception())

    else:
        return next(iter(model.skillsets))

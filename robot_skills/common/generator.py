from typing import List, Optional
from abc import ABC
import logging
import os
import stat

import robot_lang

from .templates import GeneratorTemplates, action_name, resources_in_expression

class Generator(ABC):
    def __init__(self, model: robot_lang.Model, suffix: str):
        self.__model = model
        self.__name = f"{model.name}_{suffix}"
        self.__logger = logging.getLogger(__name__)
        self._templates = GeneratorTemplates()
        self._py_pkg_folder = None
        self.__folder = None

    @property
    def name(self):
        return self.__name

    @property
    def model(self):
        return self.__model

    @property
    def folder(self):
        return self.__folder

    @property
    def _logger(self):
        return self.__logger

    def _generate_structure(self,
                            dirs: List[str] = [],
                            files: List[str] = [],
                            prefix: Optional[str] = None):
        if prefix and not os.path.isdir(prefix):
            self.__logger.error("Prefix path '%s' does not exist! Cannot generate package",
                                prefix)
            raise FileNotFoundError()
        self.__folder = os.path.join(
            prefix, self.__name) if prefix else self.__name
        self.__logger.info("Generating package %s in folder %s",
                           self.__name, self.__folder)
        try:
            os.mkdir(self.__folder)
        except FileExistsError:
            # Overidding all files but hooks/conversions
            self.__logger.info("Package folder %s already exists.",
                               self.__folder)
        for d in dirs:
            try:
                os.mkdir(os.path.join(self.__folder, d))
                self.__logger.debug("Folder %s created", d)
            except FileExistsError:
                self.__logger.debug("Folder %s already exists", d)
        for f in files:
            try:
                os.open(os.path.join(self.__folder, f),
                        os.O_WRONLY | os.O_CREAT,
                        stat.S_IRUSR | stat.S_IWUSR | stat.S_IRGRP | stat.S_IROTH)
                self.__logger.debug("File %s created", f)
            except FileNotFoundError as ex:
                self.__logger.error("Missing folder to create file %s", f)
                raise ex

    def _generate_file(self, path, template, **kwargs):
        self.__logger.debug("Generating file %s", path)
        f = open(os.path.join(self.folder, path), 'w')
        f.write(self._templates(template).render(kwargs))
        f.write('\n')
        f.close()

    def _generate_protected_file(self, path, template, **kwargs):
        flags = os.O_WRONLY | os.O_CREAT | os.O_EXCL  # Refer to "man 2 open".
        mode = stat.S_IRWXU | stat.S_IRWXG | stat.S_IRWXO  # This is 0o777.
        umask = 0o777 ^ mode  # Prevents always downgrading umask to 0.

        self.__logger.debug("Generating protected file %s", path)
        umask_original = os.umask(umask)
        try:
            fdesc = os.open(os.path.join(self.folder, path), flags, mode)
        except FileExistsError:
            self.__logger.warning(
                "File %s already exists. Not overriding!", path)
            return
        finally:
            os.umask(umask_original)
        f = os.fdopen(fdesc, "w")
        f.write(self._templates(template).render(kwargs))
        f.write('\n')
        f.close()

    def _used_resources(self, skill: robot_lang.Skill):
        res = set()
        for pre in skill.preconditions:
            res |= resources_in_expression(pre.resource)
        for result in skill.results:
            res |= resources_in_expression(result.post)
        for inv in skill.invariants:
            res |= resources_in_expression(inv.resource)
        for eff in skill.effects:
            for e in eff.effects:
                res.add(e.resource)
        return sorted(list(res))
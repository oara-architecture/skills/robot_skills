import logging
import os
import sys
try:
    import colorlog
except ImportError:
    pass

def setup_logging(level=logging.DEBUG):
    root = logging.getLogger()
    root.setLevel(level)
    format      = '%(asctime)s - %(levelname)-8s - %(name)s - %(message)s'
    date_format = '%Y-%m-%d %H:%M:%S'
    if 'colorlog' in sys.modules and os.isatty(2):
        cformat = '%(log_color)s' + format
        f = colorlog.ColoredFormatter(cformat, date_format,
              log_colors = { 'DEBUG'   : 'thin_yellow',       'INFO' : 'reset',
                             'WARNING' : 'bold_yellow', 'ERROR': 'bold_red',
                             'CRITICAL': 'bold_red' })
    else:
        f = logging.Formatter(format, date_format)
    ch = logging.StreamHandler()
    ch.setFormatter(f)
    root.addHandler(ch)

def setup_from_arg(log_arg):
    # Change log level arg to logging enum
    numeric_level = getattr(logging, log_arg.upper(), None)
    if not isinstance(numeric_level, int):
        raise ValueError('Invalid log level: %s' % log_arg)
    setup_logging(level=numeric_level)

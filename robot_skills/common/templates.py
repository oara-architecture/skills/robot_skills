from typing import Optional, List
import os
from jinja2 import Template, Environment, PackageLoader, ChoiceLoader
import logging
import re

from robot_lang.ast.resource_expression import BinBoolRes, BinCompRes
from robot_lang.ast.expression import BinBool, BinComp, FunCall, UnaryBool


def resources_in_expression(expression):
    if isinstance(expression, BinBoolRes):
        return resources_in_expression(expression.left) | resources_in_expression(expression.right)
    elif isinstance(expression, BinCompRes):
        return { expression.resource }
    else:
        return set()


def functions_in_expression(expression):
    if isinstance(expression, BinBool):
        return functions_in_expression(expression.left) | functions_in_expression(expression.right)
    elif isinstance(expression, UnaryBool):
        return functions_in_expression(expression.expr)
    elif isinstance(expression, BinComp):
        return functions_in_expression(expression.left) | functions_in_expression(expression.right)
    elif isinstance(expression, FunCall):
        return { expression.function }
    else:
        return set()


def build_expression(expression):
    if isinstance(expression, BinBoolRes):
        if expression.op == '&&':
            op = 'And'
        elif expression.op == '||':
            op = 'Or'
        elif expression.op == '=>':
            op = 'Implies'
        else:
            logging.getLogger(__name__).error("BinBoolRes operator %s not recognized! %s",
                                              expression.op, expression.info)
            return
        left = build_expression(expression.left)
        right = build_expression(expression.right)

    elif isinstance(expression, BinCompRes):
        if expression.op == '==':
            op = 'Equals'
        elif expression.op == '!=':
            op = 'Diff'
        else:
            logging.getLogger(__name__).error("BinCompRes operator %s not recognized! %s",
                                              expression.op, expression.info)
            return
        left = f"'{expression.resource}'"
        right = f"'{expression.state}'"

    else:
        logging.getLogger(__name__).error("Expression %s not recognized! %s",
                                          expression, expression.info)
        return

    return f"{op}({left}, {right})"


def action_name(skill: str, suffix: str = 'Skill') -> str:
    action = skill.capitalize()
    while action.find('_') >= 0:
        i = action.find('_')
        action = action[:i] + action[i+1:].capitalize()
    return action+suffix


def cpp_type(ros_type: str) -> str:
    return f"::{ros_type.replace('/', '::')}"


def capitalized_to_underscore(string: str) -> str:
    '''cap = list(map(lambda x: x.isupper(), string))
    newstring = string[0].lower()
    i = 0
    while True:
        try:
            j = cap.index(True, i+1)
            newstring += string[i+1:j] + '_' + string[j].lower()
            i = j
        except ValueError:
            newstring += string[i+1:]
            break
    return newstring'''
    # insert an underscore before any upper case letter
    # which is not followed by another upper case letter
    value = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', string)
    # insert an underscore before any upper case letter
    # which is preseded by a lower case letter or number
    value = re.sub('([a-z0-9])([A-Z])', r'\1_\2', string)
    return value.lower()

def ros2_message_header(ros_type: str) -> str:
    i = ros_type.rfind('/')
    pkg = ros_type[:i]
    msg = ros_type[i+1:]
    return pkg + '/msg/' + capitalized_to_underscore(msg)


def ros2_message_class(ros_type: str) -> str:
    i = ros_type.rfind('/')
    pkg = ros_type[:i]
    msg = ros_type[i+1:]
    return '::' + pkg + '::msg::' + msg


def is_data_msg(ros_type: str) -> bool:
    if ros_type.startswith('std_msgs/'):
        if ros_type.endswith('16') or ros_type.endswith('32') or ros_type.endswith('64'):
            return True
        if ros_type.endswith('Bool'):
            return True
        if ros_type.endswith('String'):
            return True
    return False


class GeneratorTemplates:

    def __init__(self, template_path: Optional[List[str]] = []):
        common = PackageLoader('robot_skills', 
                                os.path.join('templates', 'common'))

        loader = ChoiceLoader([common] +
                              [PackageLoader('robot_skills',
                                             os.path.join('templates', p))
                                for p in template_path])

        self.__env = Environment(loader=loader,
                                extensions=['jinja2.ext.do'])


        self.__env.filters['action_name'] = action_name
        self.__env.filters['data_name'] = lambda x: action_name(x, "")
        self.__env.filters['cpp_type'] = cpp_type
        self.__env.filters['ros2_message_header'] = ros2_message_header
        self.__env.filters['ros2_message_class'] = ros2_message_class
        self.__env.filters['expression'] = build_expression
        self.__env.tests['data_msg'] = is_data_msg


    def add_brige_filters(self):
        from ..common.bridge_conversion import convert_goal, convert_data
        self.__env.filters['convert_goal'] = convert_goal
        self.__env.filters['convert_data'] = convert_data

    def __call__(self, template: str) -> Template:
        return self.__env.get_template(template)

class NotSupported(Exception):
    def __init__(self, element):
        super().__init__()
        self.__element = element

    def __str__(self):
        print(f"Language element {self.__element} not supported")
        info = self.__element.info
        print(f"-- {info.file}:{info.line}")


class NotRecognized(Exception):
    def __init__(self, element):
        super().__init__()
        self.__element = element

    def __str__(self):
        print(f"Language element {self.__element} not supported")
        info = self.__element.info
        print(f"-- {info.file}:{info.line}")
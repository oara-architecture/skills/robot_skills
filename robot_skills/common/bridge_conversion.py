from .logger import logging
log = logging.getLogger(__name__)

import os, sys
ROS1 = os.environ.get('ROS_ROOT')
if ROS1:
    if 'noetic' in ROS1:
        ros_distro = 'noetic'
    if 'melodic' in ROS1:
        ros_distro = 'melodic'
    if 'lunar' in ROS1:
        ros_distro = 'lunar'
    if 'kinetic' in ROS1:
        ros_distro = 'kinetic'

    for p in [p for p in sys.path if ros_distro in p]:
        sys.path.remove(p)

try:
    import rosidl_parser
    import rosidl_runtime_py
except:
    log.warn("Automatic type conversion is based on ROS2 IDL parsing. To activate it, you source your ROS2 environment")
    log.error("Bridge type conversion not activated!")

    def convert_goal(skill, pkgname):
        return "// implement your conversion function"

    def convert_data(data, pkgname):
        return "// implement your conversion function"

else:

    def copy_message_fields(message, ros1_parent="", ros2_parent="", ros1to2=True):
        code = ""
        for f, ftype in message.get_fields_and_field_types().items():
            log.debug(f"copy_message_fields {message} {f} {ftype}")
            if ftype.startswith('sequence'):
                intype = ftype[9:-1]
                code += "for (auto &i2: {p}.{f}) {{\n".format(f=f, p=(ros1_parent if ros1to2 else ros2_parent))
                code += "  {} i1;\n".format(intype.replace('/', '::'))
                code += "  " + convert_message(intype, 'i1', 'i2', False)
                code += "  {p}.{f}.push_back(i1);\n".format(f=f, p=(ros2_parent if ros1to2 else ros1_parent))
                code += "}\n"
            elif ftype.endswith(']'):
                intype = ftype[:ftype.find('[')]
                code += "for (unsigned int i = 0; i < (unsigned int){p}.{f}.size(); i++) {{\n".format(f=f, p=(ros1_parent if ros1to2 else ros2_parent))
                code += "  auto i2 = {p}.{f}[i];\n".format(f=f, p=(ros1_parent if ros1to2 else ros2_parent))
                code += "  {} i1;\n".format(intype.replace('/', '::'))
                code += "  " + convert_message(intype, 'i1', 'i2', False)
                code += "  {p}.{f}[i] = i1;\n".format(f=f, p=(ros2_parent if ros1to2 else ros1_parent))
                code += "}\n"
            elif ftype == 'builtin_interfaces/Time':
                code += "{p1}.{f}.sec = {p2}.{f}.sec;\n".format(f=f, 
                    p1=(ros2_parent if ros1to2 else ros1_parent), 
                    p2=(ros1_parent if ros1to2 else ros2_parent))
                if ros1to2:
                    code += "{p2}.{f}.nanosec = {p1}.{f}.nsec;\n".format(f=f, p1=ros1_parent, p2=ros2_parent)
                else:
                    code += "{p1}.{f}.nsec = {p2}.{f}.nanosec;\n".format(f=f, p1=ros1_parent, p2=ros2_parent)
            else:
                code += convert_message(ftype, ros1_parent+'.'+f, ros2_parent+'.'+f, ros1to2)
        return code

    """ convert messages such as A <- B """
    def convert_message(message, ros1="", ros2="", ros1to2=True):
        if '/' in message:
            package, message = message.split('/')
            nstype = rosidl_parser.definition.NamespacedType(
                namespaces=[package, 'msg'], name=message)
            msgtype = rosidl_runtime_py.import_message_from_namespaced_type(nstype)
            return copy_message_fields(msgtype, ros1, ros2, ros1to2)
        else:
            # bare assignment
            return "  {a} = {b};\n".format(a=(ros2 if ros1to2 else ros1), b=(ros1 if ros1to2 else ros2))

    def convert_goal(skill, pkgname):
        nstype = rosidl_parser.definition.NamespacedType(
            namespaces=[pkgname, 'action'], name=skill)
        msgtype = rosidl_runtime_py.import_message_from_namespaced_type(nstype)
        return copy_message_fields(msgtype.Goal, 'ros1_goal', 'ros2_goal', ros1to2=False)

    def convert_data(data, pkgname):
        nstype = rosidl_parser.definition.NamespacedType(
            namespaces=[pkgname, 'srv'], name=f"Get{data}Data")
        msgtype = rosidl_runtime_py.import_message_from_namespaced_type(nstype)
        return copy_message_fields(msgtype.Response, 'ros1_msg', 'ros2_msg', ros1to2=True)

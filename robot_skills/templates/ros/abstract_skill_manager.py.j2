from abc import ABCMeta, abstractmethod

import enum
import math
import threading
from functools import partial

import rospy
import actionlib
from rospy.service import ServiceException
from rospy.exceptions import ROSSerializationException
from actionlib_msgs.msg import GoalStatus
from std_msgs.msg import String
from std_srvs.srv import Empty
from diagnostic_msgs.srv import SelfTest, SelfTestResponse
from diagnostic_msgs.msg import DiagnosticStatus, KeyValue

from {{ pkgname }}.msg import {{ skill.name|action_name }}Action, {{ skill.name|action_name }}Result, {{ skill.name|action_name }}Feedback
{% if resources %}from {{ pkgname }}.srv import State, Transition{% endif %}

from .logic import *

@enum.unique
class Skill{{ skill.name.capitalize() }}GoalState(enum.Enum):
    S = 'Started'
    NV = 'Not Valid'
    CR = 'Checking Resources'
    NR = 'Resources Preconditions Not Matched'
    RG = 'Running'
    IG = 'Interrupting'
    RI = 'Resource Invariant Violated'
    {%- for result in skill.results %}
    T_{{ result.name.upper() }} = 'Terminating {{ result.name }}'
    S_{{ result.name.upper() }} = 'Terminated in {{ result.name }}'
    F_{{ result.name.upper() }} = 'Postcondition failure in {{ result.name }}'
    {%- endfor %}

    @classmethod
    def is_terminal(cls, state):
        if state == cls.NV: return True
        if state == cls.NR: return True
        if state == cls.RI: return True
        {%- for result in skill.results %}
        if state == cls.S_{{ result.name.upper() }}: return True
        if state == cls.F_{{ result.name.upper() }}: return True
        {%- endfor %}
        return False

class Abstract{{ skill.name|action_name }}Manager(object):
    __metaclass__ = ABCMeta

    def __init__(self):
        self.__goals = dict()
        self.__goals_msgs = dict()
        self.__goals_states = dict()
        self.__goals_progress = dict()
        self.__goals_start_time = dict()
        self.__goals_end_time = dict()

        self.__as = actionlib.ActionServer('skill/{{ skill }}',
            {{ skill.name|action_name }}Action,
            self.__goal_cb, self.__cancel_cb, auto_start=False)

        {% if resources %}
        rospy.loginfo("[{{ skill.name }}_manager] Waiting for resource manager server")
        rospy.wait_for_service('resource/internal/lock')
        rospy.wait_for_service('resource/internal/unlock')
        self.__resource_lock = rospy.ServiceProxy('resource/internal/lock', Empty, persistent=True)
        self.__resource_unlock = rospy.ServiceProxy('resource/internal/unlock', Empty, persistent=True)

        self.__resource_transition = dict()
        self.__resource_state = dict()
        self.__resource_sub = dict()
        {% for res in resources %}
        rospy.loginfo("[{{ skill.name }}_manager] Waiting for resource manager servers for {{ res }}")
        rospy.wait_for_service('resource/internal/change_{{ res }}')
        rospy.wait_for_service('resource/get_{{ res }}')
        self.__resource_transition['{{ res }}'] = rospy.ServiceProxy('resource/internal/change_{{ res }}', Transition, persistent=True)
        self.__resource_state['{{ res }}'] = rospy.ServiceProxy('resource/get_{{ res }}', State, persistent=True)
        self.__resource_sub['{{ res }}'] = rospy.Subscriber('resource/{{ res }}_state', String,
            self.__invariants_cb)
        {% endfor %}
        {% endif %}

        self.__formula_pre = dict()
        {% for pre in skill.preconditions|selectattr('resource') %}
        self.__formula_pre['{{ pre.name }}'] = {{ pre.resource|expression }}
        {% endfor %}
        self.__formula_results = dict()
        {% for result in skill.results|selectattr('post') %}
        self.__formula_results['{{ result.name }}'] = {{ result.post|expression }}
        {% endfor %}
        self.__formula_inv = dict()
        {% for inv in skill.invariants|selectattr('resource') %}
        self.__formula_inv['{{ inv.name }}'] = {{ inv.resource|expression }}
        {% endfor %}

        self.__status_service = rospy.Service('skill/{{ skill.name }}/get_status', SelfTest, self.get_status)
        rospy.loginfo("[{{ skill.name }}_manager] Created status service")

        self.__as.start()

        self.__progress_period = {{ skill.progress }}
        {% if skill.progress > 0 %}
        rospy.Timer(rospy.Duration({{ skill.progress }}), self.publish_feedbacks)
        {% endif %}
        rospy.loginfo("[{{ skill.name }}_manager] Skill manager started")

    @property
    def progress_period(self):
        return self.__progress_period

    def get_status(self, req):
        statuses = []
        statuses.append(DiagnosticStatus(level=DiagnosticStatus.OK,
            name='{{ skill.name }}',
            message='{{ pkgname }}.msg.{{ skill.name|action_name }}Action'))
        for gid, st in self.__goals_states.items():
            status = DiagnosticStatus(name='{{ skill.name }}')
            if gid in self.__goals:
                status.level = DiagnosticStatus.OK
                status.values.append(KeyValue(key='server_state', value=str(self.__goals[gid].get_goal_status().status)))
                status.values.append(KeyValue(key='server_state_text', value=self.__goals[gid].get_goal_status().text))
            else:
                status.level = DiagnosticStatus.STALE
            status.hardware_id = gid
            status.values.append(KeyValue(key='goal', value=repr(self.__goals_msgs[gid])))
            status.values.append(KeyValue(key='skill_state', value=self.__goals_states[gid].name))
            status.values.append(KeyValue(key='skill_state_text', value=self.__goals_states[gid].value))
            status.values.append(KeyValue(key='progress', value=str(self.__goals_progress[gid])))
            status.values.append(KeyValue(key='start_time', value=str(self.__goals_start_time[gid])))
            status.values.append(KeyValue(key='end_time', value=str(self.__goals_end_time[gid])))
            statuses.append(status)
        return SelfTestResponse(id='{{ skill.name }}',
            passed=DiagnosticStatus.OK, status=statuses)

    def check_pre(self):
        for label, formula in self.__formula_pre.items():
            states = dict()
            for resource in formula.symbols():
                states[resource] = self.__resource_state[resource]().state
            rospy.logdebug("[{{ skill.name }}_manager] Precondition %s states: %s",
                         label, states)
            if not formula.eval(states):
                rospy.logerr("[{{ skill.name }}_manager] Precondition %s error",
                         label)
                return False, label
        return True, ''

    {% for effect in skill.effects %}
    def __effect_{{ effect.name }}(self):
        self.on_effect_{{ effect.name }}()
        {% for eff in effect.effects %}
        try:
            r = self.__resource_transition['{{ eff.resource }}']('{{ eff.state }}')
        except (TypeError, ROSSerializationException, ServiceException) as ex:
            rospy.logerr("[{{ skill.name }}_manager] Error in resource {{ eff.resource }} change_state arguments: %s",
                         ex)
            return False, ''
        if not r.success:
            rospy.logerr("[{{ skill.name }}_manager] Error changing resource {{ eff.resource }} state: %s -> {{ eff.state }}",
                         r.state)
            return False, '{{ eff.resource }}'
        {% endfor %}
        return True, ''
    {% endfor %}

    def __invariants_cb(self, msg):
        active_goals = [gid for gid in self.__goals
                        if (self.__goals_states[gid] == Skill{{ skill.name.capitalize() }}GoalState.RG
                            or self.__goals_states[gid] == Skill{{ skill.name.capitalize() }}GoalState.IG)]
        if not active_goals:
            return

        {% if resources %}
        rospy.logdebug("[{{ skill.name }}_manager] Lock Resource Manager")
        self.__resource_lock()
        {% endif %}
        {% for inv in skill.invariants|selectattr('resource') %}
        states = dict()
        for resource in self.__formula_inv['{{ inv.name }}'].symbols():
            states[resource] = self.__resource_state[resource]().state
        if not self.__formula_inv['{{ inv.name }}'].eval(states):
            rospy.logwarn("[{{ skill.name }}_manager] Invariant {{ inv.name }} violated!")
            {% if inv.effect %}
            val, label = self.__effect_{{ inv.effect }}()
            if not val:
                rospy.logerr("[{{ skill.name }}_manager] Error applying effect {{ inv.effect }} in invariant {{ inv.name }}")
            {% endif %}
            for gid in active_goals:
                self.__goals_states[gid] = Skill{{ skill.name.capitalize() }}GoalState.RI
                self.__goals_end_time[gid] = rospy.Time.now().to_sec()
                try:
                    self.__goals[gid].set_aborted({{ skill.name|action_name }}Result(state='RI'), text='{{ inv.name }}')
                except KeyError:
                    rospy.logwarn("[{{ skill.name }}_manager] Goal %s already purged", gid)

            {% if resources %}
            rospy.logdebug("[{{ skill.name }}_manager] Unlock Resource Manager")
            self.__resource_unlock()
            {% endif %}
            return
        {% endfor %}
        {% if resources %}
        rospy.logdebug("[{{ skill.name }}_manager] Unlock Resource Manager")
        self.__resource_unlock()
        {% endif %}

    def __goal_cb(self, gh):
        gid = gh.get_goal_id().id
        goal = gh.get_goal()
        self.__goals[gid] = gh
        self.__goals_msgs[gid] = goal
        self.__goals_start_time[gid] = rospy.Time.now().to_sec()
        self.__goals_end_time[gid] = math.inf
        self.__goals_states[gid] = Skill{{ skill.name.capitalize() }}GoalState.S
        self.__goals_progress[gid] = 0.0
        rospy.loginfo("[{{ skill.name }}_manager] Validating goal %s %s", gid, goal)

        if self.validate(goal):
            rospy.loginfo("[{{ skill.name }}_manager] Goal %s validated by user callback", gid)
            gh.set_accepted(text='valid')
            self.__goals_states[gid] = Skill{{ skill.name.capitalize() }}GoalState.CR
        else:
            rospy.logwarn("[{{ skill.name }}_manager] Goal %s not validated", gid)
            gh.set_rejected({{ skill.name|action_name }}Result(state='NV'))
            self.__goals_states[gid] = Skill{{ skill.name.capitalize() }}GoalState.NV
            self.__goals_end_time[gid] = rospy.Time.now().to_sec()
            return

        {% if resources %}
        rospy.logdebug("[{{ skill.name }}_manager] Lock Resource Manager")
        self.__resource_lock()
        {% endif %}
        val, label = self.check_pre()
        if not val:
            rospy.logwarn("[{{ skill.name }}_manager] Error in precondition %s", label)
            gh.set_aborted({{ skill.name|action_name }}Result(state='NR'), text=label)
            self.__goals_states[gid] = Skill{{ skill.name.capitalize() }}GoalState.NR
            self.__goals_end_time[gid] = rospy.Time.now().to_sec()
            {% if resources %}
            rospy.logdebug("[{{ skill.name }}_manager] Unlock Resource Manager")
            self.__resource_unlock()
            {% endif %}
            return
        {% if skill.preconditions.success %}
        val, label = self.__effect_{{ skill.preconditions.success }}()
        if not val:
            rospy.logwarn("[{{ skill.name }}_manager] Error in effect %s", label)
            gh.set_aborted({{ skill.name|action_name }}Result(state='NR'), text=label)
            self.__goals_states[gid] = Skill{{ skill.name.capitalize() }}GoalState.NR
            self.__goals_end_time[gid] = rospy.Time.now().to_sec()
            {% if resources %}
            rospy.logdebug("[{{ skill.name }}_manager] Unlock Resource Manager")
            self.__resource_unlock()
            {% endif %}
            return
        {% endif %}
        {% if resources %}
        rospy.logdebug("[{{ skill.name }}_manager] Unlock Resource Manager")
        self.__resource_unlock()
        {% endif %}

        rospy.loginfo("[{{ skill.name }}_manager] Dispatching goal {}".format(gid))
        dispatch_thread = threading.Thread(target=self.on_dispatch, args=(gid, goal))
        dispatch_thread.start()
        self.__goals_states[gid] = Skill{{ skill.name.capitalize() }}GoalState.RG

    def __cancel_cb(self, gh):
        gid = gh.get_goal_id().id
        goal = gh.get_goal()
        self.__goals_states[gid] = Skill{{ skill.name.capitalize() }}GoalState.IG
        rospy.loginfo("[{{ skill.name }}_manager] Interrupting goal %s %s (current status: %s)",
                      gid, goal, gh.get_goal_status().status)
        if gh.get_goal_status().status == GoalStatus.RECALLING:
            gh.set_accepted()
        interrupt_thread = threading.Thread(target=self.on_interrupt, args=(gid, goal))
        interrupt_thread.start()

    def publish_feedbacks(self, event):
        to_remove = []
        rospy.logdebug("[{{ skill.name }}_manager] Publishing progress")
        for gid, gh in self.__goals.items():
            if self.__goals_states[gid] == Skill{{ skill.name.capitalize() }}GoalState.RG:
                progress = self.progress(gid, gh.get_goal())
                self.__goals_progress[gid] = progress
                rospy.logdebug(" - goal %s progress %s", gid, progress)
                fb = {{ skill.name|action_name }}Feedback(progress=progress)
                gh.publish_feedback(fb)
            elif Skill{{ skill.name.capitalize() }}GoalState.is_terminal(self.__goals_states[gid]):
                to_remove.append(gid)
        for gid in to_remove:
            rospy.loginfo("[{{ skill.name }}_manager] Removing terminated goal %s", gid)
            del self.__goals[gid]

    {% for result in skill.results %}
    def check_post_{{ result }}(self):
        {% if result.post %}
        states = dict()
        for resource in self.__formula_results['{{ result.name }}'].symbols():
            states[resource] = self.__resource_state[resource]().state
        rospy.logdebug("[{{ skill.name }}_manager] Check post for result {{ result }} on states %s", states)
        return self.__formula_results['{{ result.name }}'].eval(states)
        {% else %}
        return True
        {% endif %}

    def terminated_{{ result }}(self, gid):
        try:
            gh = self.__goals[gid]
        except KeyError:
            rospy.logwarn("[{{ skill.name }}_manager] Goal %s already purged", gid)
            return
        if Skill{{ skill.name.capitalize() }}GoalState.is_terminal(self.__goals_states[gid]):
            rospy.logwarn("[{{ skill.name }}_manager] Goal %s already terminated in %s",
                          gid, self.__goals_states[gid])
            return

        goal = gh.get_goal()
        if (self.__goals_states[gid] == Skill{{ skill.name.capitalize() }}GoalState.RG
            or self.__goals_states[gid] == Skill{{ skill.name.capitalize() }}GoalState.IG):
            rospy.loginfo("[{{ skill.name }}_manager] Terminating goal %s %s in result {{ result }}",
                          gid, goal)
            self.__goals_states[gid] = Skill{{ skill.name.capitalize() }}GoalState.T_{{ result.name.upper() }}

            {% if resources %}
            rospy.logdebug("[{{ skill.name }}_manager] Lock Resource Manager")
            self.__resource_lock()
            {% endif %}
            if not self.check_post_{{ result }}():
                rospy.logerr("[{{ skill.name }}_manager] Result {{ result.name }} postcondition error")
                gh.set_aborted({{ skill.name|action_name }}Result(state='F_{{ result.name.upper() }}'), text='post')
                self.__goals_states[gid] = Skill{{ skill.name.capitalize() }}GoalState.F_{{ result.name.upper() }}
                self.__goals_end_time[gid] = rospy.Time.now().to_sec()
                {% if resources %}
                rospy.logdebug("[{{ skill.name }}_manager] Unlock Resource Manager")
                self.__resource_unlock()
                {% endif %}
                return
            {% for eff in result.effects %}
            val, label = self.__effect_{{ eff }}()
            if not val:
                rospy.logerr("[{{ skill.name }}_manager] Error applying effect {{ eff }} in result {{ result.name }}")
                gh.set_aborted({{ skill.name|action_name }}Result(state='F_{{ result.name.upper() }}'), text='{{ label }}')
                self.__goals_states[gid] = Skill{{ skill.name.capitalize() }}GoalState.F_{{ result.name.upper() }}
                self.__goals_end_time[gid] = rospy.Time.now().to_sec()
                {% if resources %}
                rospy.logdebug("[{{ skill.name }}_manager] Unlock Resource Manager")
                self.__resource_unlock()
                {% endif %}
                return
            {% endfor %}
            {% if resources %}
            rospy.logdebug("[{{ skill.name }}_manager] Unlock Resource Manager")
            self.__resource_unlock()
            {% endif %}

            rospy.loginfo("[{{ skill.name }}_manager] Goal %s finished in result {{ result }}", gid)
            gh.set_succeeded({{ skill.name|action_name }}Result(state='S_{{ result.name.upper() }}'))
            self.__goals_states[gid] = Skill{{ skill.name.capitalize() }}GoalState.S_{{ result.name.upper() }}
            self.__goals_end_time[gid] = rospy.Time.now().to_sec()
        else:
            rospy.logwarn("[{{ skill.name }}_manager] Goal %s not active (%s)", gid, gh.get_goal_status().status)
    {% endfor %}

    ''' User functions must be reimplemented '''

    @abstractmethod
    def validate(self, goal):
        pass

    @abstractmethod
    def on_dispatch(self, goal_id, goal):
        pass

    @abstractmethod
    def on_interrupt(self, goal_id, goal):
        pass

    @abstractmethod
    def progress(self, goal_id, goal):
        pass

    {% for effect in skill.effects %}
    @abstractmethod
    def on_effect_{{ effect.name }}(self):
        pass
    {% endfor %}
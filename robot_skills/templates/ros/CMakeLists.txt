cmake_minimum_required(VERSION 3.5)

project({{ pkgname }})

# Default to C++14
if(NOT CMAKE_CXX_STANDARD)
  set(CMAKE_CXX_STANDARD 14)
endif()
if(CMAKE_COMPILER_IS_GNUCXX OR CMAKE_CXX_COMPILER_ID MATCHES "Clang")
  add_compile_options(-Wall -Wextra -Wno-pedantic)
endif()

# find the ROS message code generators
find_package(catkin REQUIRED
  COMPONENTS
  {% if resources or data %}roscpp{% endif %}
  {% if resources or skills or data %}message_generation{% endif %}
  actionlib
  actionlib_msgs
  std_msgs
  {% for d in deps %}{{ d }}
  {% endfor %}
)

{% if resources %}
add_service_files(
  DIRECTORY srv
  FILES
    Transition.srv
    State.srv
)
{% endif %}

{% if data %}
add_service_files(
  DIRECTORY srv
  FILES {% for d in data %}
    Get{{ d.name | capitalize }}Data.srv
    {% endfor %}
)
{% endif %}

{% if skills %}
add_action_files(
  DIRECTORY action
  FILES
    {% for sk in skills %}{{ sk.name|action_name }}.action
    {% endfor %}
)
{% endif %}
{% if setup %}
## Python package
catkin_python_setup()
{% endif %}
{% if resources or data or skills %}
## Generate added messages and services with any dependencies listed here
generate_messages(
  DEPENDENCIES
  std_msgs
  actionlib_msgs
  {% for d in deps %}{{ d }}
  {% endfor %}
)
{% endif %}


## CATKIN PACKAGE
catkin_package(
  CATKIN_DEPENDS
  actionlib
  actionlib_msgs
  {% if resources or skills or data %}
  message_generation
  message_runtime{% endif %}
  {% if resources or data %}roscpp{% endif %}
  std_msgs
  {% for d in deps %}{{ d }}
  {% endfor %}
)

include_directories(
  ${catkin_INCLUDE_DIRS}
)

{% if resources %}
add_executable({{ name }}_resource_manager src/resource_manager.cpp)
add_dependencies({{ name }}_resource_manager {{ pkgname }}_generate_messages_cpp)
target_link_libraries(
  {{ name }}_resource_manager
  ${catkin_LIBRARIES}
)
install(TARGETS {{ name }}_resource_manager
  RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
)
{% endif %}

{% if data %}
add_executable({{ name }}_data_manager src/data_manager.cpp)
add_dependencies({{ name }}_data_manager {{ pkgname }}_generate_messages_cpp)
target_link_libraries(
  {{ name }}_data_manager
  ${catkin_LIBRARIES}
)
install(TARGETS {{ name }}_data_manager
  RUNTIME DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
)
{% endif %}

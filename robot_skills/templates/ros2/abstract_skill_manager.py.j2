from abc import ABC, abstractmethod
from functools import partial
import enum
import threading
import math

import rclpy
from rclpy.callback_groups import ReentrantCallbackGroup, MutuallyExclusiveCallbackGroup
from rclpy.executors import MultiThreadedExecutor
from rclpy.node import Node
from rclpy.action import ActionServer
from rclpy.action.server import GoalResponse, GoalStatus, CancelResponse, ServerGoalHandle

from std_msgs.msg import String
from std_srvs.srv import Empty
from diagnostic_msgs.srv import SelfTest
from diagnostic_msgs.msg import DiagnosticStatus, KeyValue

from {{ pkgname }}.action import {{ skill.name|action_name }}
{% if resources %}from {{ pkgname }}.srv import State, Transition{% endif %}
from .logic import *

@enum.unique
class Skill{{ skill.name.capitalize() }}GoalState(enum.Enum):
    S = 'Started'
    NV = 'Not Valid'
    CR = 'Checking Resources'
    NR = 'Resources Preconditions Not Matched'
    RG = 'Running'
    IG = 'Interrupting'
    RI = 'Resource Invariant Violated'
    {%- for result in skill.results %}
    T_{{ result.name.upper() }} = 'Terminating {{ result.name }}'
    S_{{ result.name.upper() }} = 'Terminated in {{ result.name }}'
    F_{{ result.name.upper() }} = 'Postcondition failure in {{ result.name }}'
    {%- endfor %}

    @classmethod
    def is_terminal(cls, state):
        if state == cls.NV: return True
        if state == cls.NR: return True
        if state == cls.RI: return True
        {%- for result in skill.results %}
        if state == cls.S_{{ result.name.upper() }}: return True
        if state == cls.F_{{ result.name.upper() }}: return True
        {%- endfor %}
        return False

class Abstract{{ skill.name|action_name }}Manager(ABC, Node):

    def __init__(self, num_threads=3, **kwargs):
        Node.__init__(self, '{{ skill }}_manager', **kwargs)
        self.__action_callback_group = ReentrantCallbackGroup()
        self.__resource_callback_group = ReentrantCallbackGroup()
        self.__executor = MultiThreadedExecutor(num_threads=num_threads)

        self.__goals = dict()
        self.__goals_msgs = dict()
        self.__goals_states = dict()
        self.__goals_progress = dict()
        self.__goals_start_time = dict()
        self.__goals_end_time = dict()
        self.__results = dict()
        self.__events = dict()

        self.__as = ActionServer(self,
            {{ skill.name|action_name }},
            'skill/{{ skill.name }}',
            self.__exec_cb,
            goal_callback=self.__goal_cb,
            handle_accepted_callback=self.__accepted_cb,
            cancel_callback=self.__cancel_cb,
            callback_group=self.__action_callback_group
            )

        {% if resources %}
        self.__resource_lock = self.create_client(Empty,
            'resource/internal/lock',
            callback_group=self.__resource_callback_group)
        self.__resource_unlock = self.create_client(Empty,
            'resource/internal/unlock',
            callback_group=self.__resource_callback_group)
        self.get_logger().info("waiting connection to resource manager")
        self.__resource_lock.wait_for_service()
        self.__resource_transition = dict()
        self.__resource_state = dict()
        self.__resource_sub = dict()
        {% for res in resources %}
        self.get_logger().info("create client for resource manager {{ res }}")
        self.__resource_transition['{{ res }}'] = self.create_client(Transition,
            'resource/internal/change_{{ res }}',
            callback_group=self.__resource_callback_group)
        self.__resource_state['{{ res }}'] = self.create_client(State,
            'resource/get_{{ res }}',
            callback_group=self.__resource_callback_group)
        self.__resource_sub['{{ res }}'] = self.create_subscription(String,
            'resource/{{ res }}_state',
            self.__invariants_cb,
            10)
        {% endfor %}
        {% endif %}

        self.__formula_pre = dict()
        {% for pre in skill.preconditions|selectattr('resource') %}
        self.__formula_pre['{{ pre.name }}'] = {{ pre.resource|expression }}
        {% endfor %}
        self.__formula_results = dict()
        {% for result in skill.results|selectattr('post') %}
        self.__formula_results['{{ result.name }}'] = {{ result.post|expression }}
        {% endfor %}
        self.__formula_inv = dict()
        {% for inv in skill.invariants|selectattr('resource') %}
        self.__formula_inv['{{ inv.name }}'] = {{ inv.resource|expression }}
        {% endfor %}

        self.__status_service = self.create_service(SelfTest,
            'skill/{{ skill.name }}/get_status',
            self.get_status,
            callback_group=self.__action_callback_group)

        self.__progress_period = {{ skill.progress }}
        {% if skill.progress > 0 %}
        self.__timer = self.create_timer({{ skill.progress }}, self.publish_feedbacks,
                                         callback_group=self.__action_callback_group)
        {% endif %}
        self.get_logger().info("skill manager started")

    def __id(self, gh):
        return str(gh.goal_id.uuid)

    @property
    def progress_period(self):
        return self.__progress_period

    def spin(self):
        rclpy.spin(self, self.__executor)

    def spin_until_future_complete(self, future):
        return self.__executor.spin_until_future_complete(future)

    def get_status(self, request, response):
        response.status = []
        response.status.append(DiagnosticStatus(level=DiagnosticStatus.OK,
            name='{{ skill.name }}',
            message='{{ pkgname }}.action.{{ skill.name|action_name }}'))
        for gid, st in self.__goals_states.items():
            status = DiagnosticStatus(name='{{ skill.name }}')
            if gid in self.__goals:
                status.level = DiagnosticStatus.OK
                status.values.append(KeyValue(key='server_state', value=str(self.__goals[gid].status)))
                #status.values.append(KeyValue(key='server_state_text', value=self.__goals[gid].get_goal_status().text))
            else:
                status.level = DiagnosticStatus.STALE
            status.hardware_id = gid
            status.values.append(KeyValue(key='start_time', value=str(self.__goals_start_time[gid])))
            status.values.append(KeyValue(key='end_time', value=str(self.__goals_end_time[gid])))            
            status.values.append(KeyValue(key='goal', value=repr(self.__goals_msgs[gid])))
            status.values.append(KeyValue(key='skill_state', value=st.name))
            status.values.append(KeyValue(key='skill_state_text', value=st.value))
            status.values.append(KeyValue(key='progress', value=str(self.__goals_progress[gid])))
            response.status.append(status)
        response.id = '{{ skill.name }}'
        response.passed = DiagnosticStatus.OK
        return response

    def check_pre(self):
        for label, formula in self.__formula_pre.items():
            states = dict()
            for res in formula.symbols():
                future = self.__resource_state[res].call_async(State.Request())
                self.__executor.spin_until_future_complete(future)
                self.get_logger().debug("received response from resource manager")
                states[res] = future.result().state
            if not formula.eval(states):
                self.get_logger().error("precondition {} error".format(label))
                return False, label
        return True, ''

    {% for effect in skill.effects %}
    def __effect_{{ effect.name }}(self):
        self.on_effect_{{ effect.name }}()
        {% for eff in effect.effects %}
        future = self.__resource_transition['{{ eff.resource }}'].call_async(Transition.Request(
            target='{{ eff.state }}'))
        self.__executor.spin_until_future_complete(future)
        self.get_logger().debug("received response from resource manager")
        r = future.result()
        if not r.success:
            self.get_logger().error("error changing resource {{ eff.resource }} state: {} -> {{ eff.state }}".format(r.state))
            return False, '{{ eff.resource }}'
        {% endfor %}
        return True, ''
    {% endfor %}

    {% if resources %}
    def __resource_manager_lock(self):
        self.get_logger().info("locking resource manager")
        future = self.__resource_lock.call_async(Empty.Request())
        self.__executor.spin_until_future_complete(future)
        self.get_logger().debug("resource manager locked")
        
    def __resource_manager_unlock(self):
        self.get_logger().info("unlocking resource manager")
        future = self.__resource_unlock.call_async(Empty.Request())
        self.__executor.spin_until_future_complete(future)
        self.get_logger().debug("resource manager unlocked")
    {% endif %}

    def __invariants_cb(self, msg):
        active_goals = [gid for gid in self.__goals
                        if (self.__goals_states[gid] == Skill{{ skill.name.capitalize() }}GoalState.RG
                            or self.__goals_states[gid] == Skill{{ skill.name.capitalize() }}GoalState.IG)]
        if not active_goals:
            return

        {% if resources %}
        self.__resource_manager_lock()
        {% endif %}
        {% for inv in skill.invariants|selectattr('resource') %}
        states = dict()
        for res in self.__formula_inv['{{ inv.name }}'].symbols():
            future = self.__resource_state[res].call_async(State.Request())
            self.__executor.spin_until_future_complete(future)
            self.get_logger().debug("received response from resource manager")
            states[res] = future.result().state

        if not self.__formula_inv['{{ inv.name }}'].eval(states):
            self.get_logger().warn("invariant {{ inv.name }} violated!")
            {% if inv.effect %}
            val, label = self.__effect_{{ inv.effect }}()
            if not val:
                self.get_logger().error("error applying effect {{ inv.effect }} in invariant {{ inv.name }}")
            {% endif %}
            for gid in active_goals:
                try:
                    gh = self.__goals[gid]
                except KeyError:
                    self.get_logger().warn("goal {} already purged".format(gid))
                    continue
                self.__results[gid] = "RI({{ inv.name }})"
                self.__goals_end_time[gid], _ = self.get_clock().now().seconds_nanoseconds()
                gh.abort()
                self.__goals_states[gid] = Skill{{ skill.name.capitalize() }}GoalState.RI
                self.__events[gid].set()
            {% if resources %}
            self.__resource_manager_unlock()
            {% endif %}
            return
        {% endfor %}
        {% if resources %}
        self.__resource_manager_unlock()
        {% endif %}

    def __goal_cb(self, goal_request):
        self.get_logger().info("validating goal {}".format(goal_request))
        #self.__goals_states[gid] = Skill{{ skill.name.capitalize() }}GoalState.S

        if self.validate(goal_request):
            self.get_logger().info("goal {} validated".format(goal_request))
            #self.__goals_states[gid] = Skill{{ skill.name.capitalize() }}GoalState.CR
            return GoalResponse.ACCEPT
        else:
            self.get_logger().warn("goal {} not validated".format(goal_request))
            #self.__goals_states[gid] = Skill{{ skill.name.capitalize() }}GoalState.NV
            return GoalResponse.REJECT # terminal state NV

    def __accepted_cb(self, gh):
        gid = self.__id(gh)
        self.__goals[gid] = gh
        self.__goals_msgs[gid] = gh.request
        self.__goals_progress[gid] = 0.0
        self.__goals_states[gid] = Skill{{ skill.name.capitalize() }}GoalState.CR
        self.__goals_start_time[gid], _ = self.get_clock().now().seconds_nanoseconds()
        self.__goals_end_time[gid] = math.inf
        gh.execute()

    def __cancel_cb(self, gh):
        self.get_logger().info("interrupting goal {} {}".format(gh.goal_id, gh.request))
        self.__goals_states[self.__id(gh)] = Skill{{ skill.name.capitalize() }}GoalState.IG
        self.__executor.create_task(self.on_interrupt, self.__id(gh), gh.request)
        return CancelResponse.ACCEPT

    def __exec_cb(self, gh):
        gid = self.__id(gh)
        {% if resources %}
        self.__resource_manager_lock()
        val, res = self.check_pre()
        if not val:
            self.__resource_manager_unlock()
            self.get_logger().warn("error in precondition {}".format(res))
            self.__results[gid] = "NR({})".format(res)
            self.__goals_states[gid] = Skill{{ skill.name.capitalize() }}GoalState.NR
            self.__goals_end_time[gid], _ = self.get_clock().now().seconds_nanoseconds()
            return {{ skill.name|action_name }}.Result(state='NR')
        {% if skill.preconditions.success %}
        val, res = self.__effect_{{ skill.preconditions.success }}()
        if not val:
            self.__resource_manager_unlock()
            self.get_logger().warn("error in effect {}".format(res))
            self.__results[gid] = "NR({})".format(res)
            self.__goals_states[gid] = Skill{{ skill.name.capitalize() }}GoalState.NR
            self.__goals_end_time[gid], _ = self.get_clock().now().seconds_nanoseconds()
            return {{ skill.name|action_name }}.Result(state='NR')
        {% endif %}
        self.__resource_manager_unlock()
        {% endif %}
        self.__events[gid] = threading.Event()
        self.get_logger().info("dispatching goal {}".format(gh.goal_id))
        self.__goals_states[gid] = Skill{{ skill.name.capitalize() }}GoalState.RG
        self.on_dispatch(gid, gh.request)
        self.__events[gid].wait()
        status = self.__goals_states[gid]
        if not Skill{{ skill.name.capitalize() }}GoalState.is_terminal(status):
            self.get_logger().error("[goal {}: execution callback finished with status {}".format(gh.status, gid))
            return {{ skill.name|action_name }}.Result(state='UNKNOWN')
        else:
            result = self.__results[gid]
            self.get_logger().info("goal {}: returning result {}".format(gid, result))
            self.__goals_end_time[gid], _ = self.get_clock().now().seconds_nanoseconds()
            return {{ skill.name|action_name }}.Result(state=result)

    def publish_feedbacks(self):
        to_remove = []
        self.get_logger().debug("publishing progress")
        for gid, gh in self.__goals.items():
            status = self.__goals_states[gid]
            if status == Skill{{ skill.name.capitalize() }}GoalState.RG:
                progress = self.progress(gid, gh.request)
                self.__goals_progress[gid] = progress
                self.get_logger().debug(" - goal {} progress {}".format(gid, progress))
                gh.publish_feedback({{ skill.name|action_name }}.Feedback(progress=progress))
            elif Skill{{ skill.name.capitalize() }}GoalState.is_terminal(status):
                to_remove.append(gid)
        for gid in to_remove:
            self.get_logger().info("removing terminated goal {}".format(gid))
            del self.__goals[gid]
            del self.__events[gid]

    {% for result in skill.results %}
    def check_post_{{ result }}(self):
        {% if result.post %}
        states = dict()
        for res in self.__formula_results['{{ result.name }}'].symbols():
            future = self.__resource_state[res].call_async(State.Request())
            self.__executor.spin_until_future_complete(future)
            self.get_logger().debug("received response from resource manager")
            states[res] = future.result().state
        self.get_logger().debug("check post of result {{ result.name }} on states {}".format(states))
        return self.__formula_results['{{ result.name }}'].eval(states)
        {% else %}
        return True
        {% endif %}

    def terminated_{{ result }}(self, gid):
        try:
            gh = self.__goals[gid]
        except KeyError:
            self.get_logger().warn("goal {} already purged".format(gid))
            return
        status = self.__goals_states[self.__id(gh)]
        if Skill{{ skill.name.capitalize() }}GoalState.is_terminal(status):
            self.get_logger().warn("goal {} already terminated in {}".format(gid, status))
            return
        
        if (status == Skill{{ skill.name.capitalize() }}GoalState.RG or status == Skill{{ skill.name.capitalize() }}GoalState.IG):
            self.get_logger().info("terminating goal {} {} in result {{ result }}".format(gid, gh.request))
            self.__goals_states[gid] = Skill{{ skill.name.capitalize() }}GoalState.T_{{ result.name.upper() }}
            {% if resources %}
            self.__resource_manager_lock()
            if not self.check_post_{{ result }}():
                self.get_logger().error("result {{ result.name }} postcondition error")
                self.__results[gid] = 'F_{{ result.name.upper() }}'
                self.__goals_states[gid] = Skill{{ skill.name.capitalize() }}GoalState.F_{{ result.name.upper() }}
                gh.abort()
                self.__events[gid].set()
                self.__resource_manager_unlock()
                return
            {% endif %}
            {% for eff in result.effects %}
            val, label = self.__effect_{{ eff }}()
            if not val:
                self.get_logger().error("error applying effect {{ eff }} in result {{ result.name }}")
                self.__results[gid] = 'F_{{ result.name.upper() }}'
                self.__goals_states[gid] = Skill{{ skill.name.capitalize() }}GoalState.F_{{ result.name.upper() }}
                gh.abort()
                self.__events[gid].set()
                {% if resources %}
                self.__resource_manager_unlock()
                {% endif %}
                return
            {% endfor %}
            {% if resources %}
            self.__resource_manager_unlock()
            {% endif %}
            self.get_logger().info("goal {} succeeded in result {{ result }}".format(gid))
            self.__results[gid] = 'S_{{ result.name.upper() }}'
            self.__goals_states[gid] = Skill{{ skill.name.capitalize() }}GoalState.S_{{ result.name.upper() }}
            gh.succeed()
            self.__events[gid].set()
        else:
            self.get_logger().warn("goal {} not active ({})".format(gid, gh.status))
    {% endfor %}

    ''' User functions must be reimplemented '''

    @abstractmethod
    def validate(self, goal_request):
        pass

    @abstractmethod
    def on_dispatch(self, goal_id, goal):
        pass

    @abstractmethod
    def on_interrupt(self, goal_id, goal):
        pass

    @abstractmethod
    def progress(self, goal_id, goal):
        pass

    {% for effect in skill.effects %}
    @abstractmethod
    def on_effect_{{ effect.name }}(self):
        pass
    {% endfor %}
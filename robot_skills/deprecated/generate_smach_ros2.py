#!/usr/bin/env python3
import os
from .utils.generator import *
from .utils.logger import logging

class SmachGenerator:
    def __init__(self, model):
        self.model = model

    def generate(self, prefix=None):
        # Create folders
        name = self.model.name + "_smach_controllers"
        out = generate_structure(name,
            dirs = ['src', os.path.join('src', name), 'resource'],
            files=[os.path.join('src', name, '__init__.py'), os.path.join('resource', name)],
            prefix=prefix)
        if not out:
            return
        # Generate package.xml
        generate_file("package.xml", "ros2_python_package.xml", out,
            pkgname=name, version='1.0.0', deps=['smach_msgs', 'smach_ros'])
        # Generate setup.py
        generate_file("setup.py", "ros2_setup.python", out,
            pkgname=name, version='1.0.0')
        generate_file("setup.cfg", "ros2_setup.cfg", out, pkgname=name)
        # Generate Skill Managers
        for skill in self.model.skills:
            generate_file(os.path.join('src', name, "{}_smach_controller.py".format(skill)),
                "smach_ros2_controller.rospy", out,
                manager="{}_managers".format(self.model.name),
                skill=skill)

if __name__ == '__main__':
    import sys
    from .utils.main import parse, build_parser

    parser = build_parser('Generate ROS SMACH controllers', with_types=False, with_deps=False)
    model, _, args = parse(parser)
    log = logging.getLogger(__name__)

    try:
        gen = SmachGenerator(get_robot_model(model, args.robot))
        gen.generate(prefix=args.prefix)
    except Exception as ex:
        log.error("Cannot generate SMACH library from the model")
        log.error(ex)
        sys.exit()

#!/usr/bin/env python3
import os
from .utils.generator import *
from .utils.logger import logging

class SmachGenerator:
    def __init__(self, model):
        self.model = model

    def generate(self, prefix=None, type_mapping=dict(), deps=[]):
        # Create folders
        name = self.model.name + "_smach_controllers"
        out = generate_structure(name,
            dirs = ['src', os.path.join('src', name)],
            files=[os.path.join('src', name, '__init__.py')],
            prefix=prefix)
        if not out:
            return
        # Compute list of message dependencies
        deps = set(deps + [v[:v.find('/')] for v in type_mapping.values() if v.find('/') > 0])
        # Generate package.xml
        generate_file("package.xml", "ros1_package.xml", out,
            pkgname=name, version='1.0.0', deps=deps, manager=False)
        # Generate CMakeLists.txt
        generate_file("CMakeLists.txt", "ros1_CMakeLists.txt", out,
            pkgname=name,
            deps=deps,
            setup=True)
        # Generate setup.py
        generate_file("setup.py", "smach_setuppy", out,
            pkgname=name)
        # Generate Skill Managers
        for skill in self.model.skills:
            generate_file(os.path.join('src', name, "{}_smach_controller.py".format(skill)),
                "smach_controller.rospy", out,
                manager="{}_managers".format(self.model.name),
                skill=skill)

if __name__ == '__main__':
    import sys
    from .utils.main import parse, build_parser

    parser = build_parser('Generate ROS SMACH controllers', False)
    model, _, args = parse(parser)
    log = logging.getLogger(__name__)

    try:
        gen = SmachGenerator(get_robot_model(model, args.robot))
        gen.generate(prefix=args.prefix, deps=args.deps)
    except Exception as ex:
        log.error("Cannot generate SMACH library from the model")
        log.error(ex)
        sys.exit()

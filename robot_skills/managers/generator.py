from typing import List, Optional
from abc import ABC
import logging
import os
import stat

import robot_lang

from ..common.generator import Generator
from ..common.templates import action_name
from ..common.templates import GeneratorTemplates

class ManagerGenerator(Generator):

    def __init__(self, model: robot_lang.Model):
        Generator.__init__(self, model, 'managers')

    def generate(self, prefix, type_mapping, deps,
                author, email, license, with_managers: bool = True):
        # Create folders
        self._logger.debug("Generating package structure")
        self._generate_structure(
            dirs=['src', 'srv', 'action', 'launch', 'scripts', 'extra',
                  os.path.join('scripts', 'managers'),
                  self.name],
            files=[os.path.join('scripts', 'managers', '__init__.py'),
                   os.path.join(self.name, '__init__.py')],
            prefix=prefix)

        # Convenient .gitignore
        self._generate_file(".gitignore", "gitignore")

        # Compute list of message dependencies
        deps = list(sorted(set(deps + 
                   ['std_msgs', 'diagnostic_msgs', 'std_srvs'] + 
                   [v[:v.find('/')] for v in type_mapping.values() 
                                    if v.find('/') > 0])))
        self._logger.debug("Computed dependencies: %s", deps)

        # Generate package.xml
        self._generate_file("package.xml", "package.xml",
                            pkgname=self.name, 
                            version='1.0.0', 
                            deps=deps,
                            author=author,
                            email=email,
                            license=license,
                            message=True,
                            cpp=True)

        # Generate CMakeLists.txt
        self._generate_file("CMakeLists.txt", "CMakeLists.txt",
                            pkgname=self.name,
                            deps=deps,
                            setup=False,
                            resources=self.model.resources,
                            data=self.model.data,
                            skills=self.model.skills,
                            name=self.model.name)

        self._logger.info("Generating message/service/action files")
        # Generate Services
        if self.model.resources:
            self._logger.debug("Model has resources: generating service files")
            for srv in ['State', 'Transition']:
                self._generate_file(os.path.join("srv", f"{srv}.srv"), 
                                    f"{srv}.srv")
        if self.model.data:
            for data in self.model.data:
                self._logger.debug("Data getter service for %s", data)
                self._generate_file(os.path.join("srv", f"Get{action_name(data.name, suffix='')}Data.srv"), 
                                    "DataGetter.srv",
                                    data_type=data.type,
                                    typemap=type_mapping)

        # Generate Actions
        for sk in self.model.skills:
            self._logger.debug("Skill Action for %s", sk)
            self._generate_file(os.path.join("action", action_name(sk.name)+".action"),
                                "SkillInputs.action",
                                inputs=sk.inputs, 
                                typemap=type_mapping)

        # Data Manager
        if self.model.data:
            self._logger.info("Generating data manager")
            self._generate_file(os.path.join("src", "data_manager.cpp"),
                                "data_manager.cpp.j2",
                                pkgname=self.name,
                                data=self.model.data,
                                skillset=self.model.name,
                                typemap=type_mapping)

        # Resource Manager
        if self.model.resources:
            self._logger.info("Generating resource manager")
            self._generate_file(os.path.join('src', "resource_manager.cpp"),
                                "resource_manager.cpp.j2",
                                pkgname=self.name, 
                                skillset=self.model.name,
                                resources=self.model.resources,
                                number_skills=len(self.model.skills))
            
        # Skill Managers
        self._logger.info("Generating skill managers package")
        self._generate_file(os.path.join(self._py_pkg_folder, "logic.py"),
                            "logic.py.j2")
        for skill in self.model.skills:
            self._logger.info("Generating manager for skill %s", skill.name)
            used_resources = self._used_resources(skill)
            self._generate_file(os.path.join(self._py_pkg_folder, f"{skill}_abstract_manager.py"),
                                "abstract_skill_manager.py.j2", 
                                pkgname=self.name,
                                resources=used_resources,
                                skill=skill)
            self._logger.info("Generating mock manager for skill %s", skill.name)
            self._generate_file(os.path.join(self._py_pkg_folder, f"{skill}_abstract_mock.py"),
                                "abstract_skill_mock.py.j2", 
                                pkgname=self.name,
                                resources=used_resources,
                                skill=skill)
            self._generate_protected_file(os.path.join('scripts', f"{skill}_mock_manager.py"),
                                          "skill_manager_mock.py.j2",
                                          pkgname=self.name,
                                          skill=skill)
            if with_managers:
                self._generate_protected_file(os.path.join('scripts', f"{skill}_manager.py"),
                                              "skill_manager.py.j2",
                                              pkgname=self.name,
                                              skill=skill)

        # Extra Model file
        self._logger.info("Generating extra model file")
        self._generate_file(os.path.join('extra', f'{self.model}.rl'), "model.rl.j2", 
                            model=self.model.pretty(),
                            types=type_mapping)
        for resource in self.model.resources:
            robot_lang.ResourceDot(resource).write(os.path.join(self.folder, 'extra', f'{resource}_resource.dot'))
        for skill in self.model.skills:
            robot_lang.SkillDot(skill).write(os.path.join(self.folder, 'extra', f'{skill}_skill.dot'))            


class Ros1ManagerGenerator(ManagerGenerator):

    def __init__(self, model: robot_lang.Model):
        ManagerGenerator.__init__(self, model)
        self._templates = GeneratorTemplates(['ros'])
        self._py_pkg_folder = os.path.join('scripts', 'managers')

    def generate(self, prefix, type_mapping, deps,
                 author, email, license, with_managers: bool = True):
        ManagerGenerator.generate(
            self, prefix, type_mapping, deps, author, email, license, with_managers=with_managers)
        # Launch file
        if with_managers:
            self._logger.info("Generating launch file")
            self._generate_file(os.path.join('launch', f"{self.model.name}_managers.launch"),
                                "skillset_managers.launch.j2",
                                pkgname=self.name,
                                skillset=self.model,
                                mock=False)
        # Launch file for mocks
        self._logger.info("Generating mock launch file")
        self._generate_file(os.path.join('launch', f"{self.model.name}_mocks.launch"),
                            "skillset_managers.launch.j2",
                            pkgname=self.name,
                            skillset=self.model,
                            mock=True)
        # Launch file for monitoring
        self._logger.info("Generating monitoring launch file")
        self._generate_file(os.path.join('launch', f"{self.model.name}_monitoring.launch"),
                            "monitoring.launch.j2",
                            resources=self.model.resources)
        # Extra Options
        self._logger.info("Generating extra options file")
        self._generate_file(os.path.join('extra', "options.conf"),
                            "options.j2",
                            pkg=self.name,
                            ros='ros1',
                            prefix=prefix,
                            types=type_mapping,
                            author=author,
                            license=license,
                            email=email,
                            mocks=not with_managers,
                            deps=deps)


class Ros2ManagerGenerator(ManagerGenerator):

    def __init__(self, model: robot_lang.Model):
        ManagerGenerator.__init__(self, model)
        self._templates = GeneratorTemplates(['ros2'])
        self._py_pkg_folder = self.name

    def generate(self, prefix, type_mapping, deps,
                 author, email, license, with_managers: bool = True):
        ManagerGenerator.generate(
            self, prefix, type_mapping, deps, author, email, license, with_managers=with_managers)
        # Launch file
        if with_managers:
            self._logger.info("Generating launch file")
            self._generate_file(os.path.join('launch', f"{self.model.name}_managers.launch.py"),
                                "skillset_managers.launch.py.j2",
                                pkgname=self.name,
                                skillset=self.model,
                                mock=False,
                                output="log")
            self._generate_file(os.path.join('launch', f"{self.model.name}_managers_screen.launch.py"),
                                "skillset_managers.launch.py.j2",
                                pkgname=self.name,
                                skillset=self.model,
                                mock=False,
                                output="both")
        # Launch file for mocks
        self._logger.info("Generating mock launch file")
        self._generate_file(os.path.join('launch', f"{self.model.name}_mocks.launch.py"),
                            "skillset_managers.launch.py.j2",
                            pkgname=self.name,
                            skillset=self.model,
                            mock=True,
                            output="both")
        # Launch file for monitoring
        self._logger.info("Generating monitoring launch file")
        self._generate_file(os.path.join('launch', f"{self.model.name}_monitoring.launch.py"),
                            "monitoring.launch.py.j2",
                            resources=self.model.resources)
        # Extra Options
        self._logger.info("Generating extra options file")
        self._generate_file(os.path.join('extra', "options.conf"),
                            "options.j2",
                            pkg=self.name,
                            ros='ros2',
                            prefix=prefix,
                            types=type_mapping,
                            author=author,
                            license=license,
                            email=email,
                            mocks=not with_managers,
                            deps=deps)

import sys
import argparse
import json
import yaml
import traceback

import robot_lang

from ..common.logger import setup_from_arg, logging
from ..common.model import get_model
from .generator import Ros1ManagerGenerator, Ros2ManagerGenerator

log = logging.getLogger('robot_skills.managers')


def main():
    parser = argparse.ArgumentParser(
        prog='python3 -m robot_skills.managers', description='Generate ROS managers')
    parser.add_argument('model', type=str, nargs='+', help='model files')
    parser.add_argument('-s', '--skillset', 
        help='Skillset name. Must be specified if the input model contains several skillsets.')
    parser.add_argument('-l', '--log', 
        help='Log level (default: info)', default='info')
    parser.add_argument('--prefix', 
        help='Prefix of the generation path folder. Typically, the path to a workspace/src.')
    parser.add_argument('--deps', default=[], action="append",
                        help='ROS package dependency. To add multiple dependencies, you must repeat the --deps option')

    parser.add_argument('-t', '--types', type=json.loads,
                        help='Mapping from model types to generation target')
    parser.add_argument('--types-json', action="append", type=str,
                        help='Json file containing the mapping from model types to generation target')
    parser.add_argument('--types-yaml', action="append", type=str,
                        help='YAML file containing the mapping from model types to generation target')

    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument("-1", "--ros1", dest="ros2", action='store_false',
                       help="generate managers for ROS1")
    group.add_argument("-2", "--ros2", dest="ros2", action='store_true',
                       help="generate managers for ROS2")

    parser.add_argument('--author', type=str, default='robot_skills',
                        help='Specify package author')
    parser.add_argument('--email', type=str, default='robot_skills@oara.onera.fr',
                        help='Specify author email')
    parser.add_argument('--license', type=str, default='BSD License',
                        help='Specify package license')

    parser.add_argument("-m", "--mocks-only", action='store_true',
                       help="generate only mock managers")

    args = parser.parse_args()
    setup_from_arg(args.log)

    try:
        model = robot_lang.parse_all(args.model)
    except FileNotFoundError:
        log.error("Model file %s not found", args.model)
        sys.exit(1)
    except robot_lang.RobotLangException as ex:
        log.error(ex)
        sys.exit(1)
        
    try:
        typemap = args.types if args.types else {}
        if args.types_json:
            for f in args.types_json:
                log.info("Load type mapping from Json file {}".format(f))
                typemap.update(json.load(f))
        if args.types_yaml:
            for f in args.types_yaml:
                log.info("Load type mapping from Yaml file {}".format(f))
                typemap.update(yaml.load(open(f, 'r'), Loader=yaml.FullLoader))
    except AttributeError:
        typemap = None

    try:
        model = get_model(model, args.skillset)

        if args.ros2:
            gen = Ros2ManagerGenerator(model)
        else:
            gen = Ros1ManagerGenerator(model)

        gen.generate(prefix=args.prefix, 
                     type_mapping=typemap, 
                     deps=args.deps,
                     author=args.author,
                     email=args.email,
                     license=args.license,
                     with_managers=not args.mocks_only)

    except StopIteration:
        log.error("The model does not contain any skillset: nothing is generated")
        sys.exit(1)

    except Exception:
        log.error("Cannot generate managers from the model")
        traceback.print_exc()
        sys.exit(1)


if __name__ == '__main__':
    main()

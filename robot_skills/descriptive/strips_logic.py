import functools
import operator
import robot_lang

from ..common.generator import Generator
from ..common.templates import resources_in_expression, functions_in_expression
from ..common.exceptions import NotSupported, NotRecognized

def flatten(l):
    return functools.reduce(operator.iconcat, l, [])

class StripsLogicGenerator(Generator):
    def __init__(self, model: robot_lang.Model):
        super().__init__(model, 'strips')

    def __build_resource_pre(self, expression):
        if isinstance(expression, robot_lang.ast.resource_expression.BinBoolRes):
            if expression.op == '&&':
                left = self.__build_resource_pre(expression.left)
                right = self.__build_resource_pre(expression.right)
                return left + right
            elif expression.op == '||':
                raise NotSupported(expression)
            elif expression.op == '=>':
                raise NotSupported(expression)
            else:
                raise NotRecognized(expression)

        elif isinstance(expression, robot_lang.ast.resource_expression.BinCompRes):
            if expression.op == '==':
                return [f"{expression.resource.lower()}({self.skillset}, {expression.state.lower()})"]
            elif expression.op == '!=':
                return [f"not( {expression.resource.lower()}({self.skillset}, {expression.state.lower()}) )"]
            else:
                raise NotRecognized(expression)

        else:
            raise NotRecognized(expression)

    def __build_pre_expr(self, expression):
        if isinstance(expression, robot_lang.ast.expression.BinBool):
            if expression.op == '&&':
                left = self.__build_pre_expr(expression.left)
                right = self.__build_pre_expr(expression.right)
                return [left, right]
            elif expression.op == '||':
                raise NotSupported(expression)
            elif expression.op == '=>':
                raise NotSupported(expression)
            else:
                raise NotRecognized(expression)

        elif isinstance(expression, robot_lang.ast.expression.UnaryBool):
            if expression.op == '!':
                return self.__build_pre_expr(expression.expr)
            else:
                raise NotRecognized(expression)

        elif isinstance(expression, robot_lang.ast.expression.BinComp):
            left = self.__build_pre_expr(expression.left)
            
            if isinstance(expression.right, robot_lang.ast.expression.BoolValue):
                if expression.right.value:
                    if expression.op == '==':
                        return left
                    elif expression.op == '!=':
                        return f"not({left})"
                else:
                    if expression.op == '==':
                        return f"not({left})"
                    elif expression.op == '!=':
                        return left

            else:
                right = self.__build_pre_expr(expression.right)
                if expression.op == '==':
                    return f"=({left}, {right})"
                elif expression.op == '!=':
                    return f"dif({left}, {right})"
                else:
                    raise NotRecognized(expression)

        elif isinstance(expression, robot_lang.ast.expression.BoolValue):
            return f"{expression.value}"

        elif isinstance(expression, robot_lang.ast.expression.Number):
            return f"{expression.value}"

        elif isinstance(expression, robot_lang.ast.expression.Identifier):
            return f"{expression.name.capitalize()}"

        elif isinstance(expression, robot_lang.ast.expression.FunCall):
            return f"{expression.function}({', '.join([self.skillset] + list(map(self.__build_pre_expr, expression.parameters)))})"

        else:
            raise NotRecognized(expression)

    def __build_post_expr(self, expression):
        if isinstance(expression, robot_lang.ast.expression.BinBool):
            if expression.op == '&&':
                delleft, addleft = self.__build_post_expr(expression.left)
                delright, addright = self.__build_post_expr(expression.right)
                return [delleft, delright], [addleft, addright]
            else:
                raise NotSupported(expression)

        elif isinstance(expression, robot_lang.ast.expression.UnaryBool):
            if expression.op == '!':
                dels, adds = self.__build_post_expr(expression.expr)
                return adds, dels
            else:
                raise NotSupported(expression)

        elif isinstance(expression, robot_lang.ast.expression.BinComp):
            delleft, addleft = self.__build_post_expr(expression.left)
            
            if isinstance(expression.right, robot_lang.ast.expression.BoolValue):
                if expression.right.value:
                    if expression.op == '==':
                        return delleft, addleft
                    elif expression.op == '!=':
                        return addleft, delleft
                else:
                    if expression.op == '==':
                        return addleft, delleft
                    elif expression.op == '!=':
                        return delleft, addleft

            else:
                delright, addright = self.__build_post_expr(expression.right)
                if expression.op == '==':
                    return [], f"=({addleft}, {addright})"
                elif expression.op == '!=':
                    return [], f"dif({addleft}, {addright})"
                else:
                    raise NotRecognized(expression)

        elif isinstance(expression, robot_lang.ast.expression.BoolValue):
            return [], f"{expression.value}"

        elif isinstance(expression, robot_lang.ast.expression.Number):
            return [], f"{expression.value}"

        elif isinstance(expression, robot_lang.ast.expression.Identifier):
            return [], f"{expression.name.capitalize()}"

        elif isinstance(expression, robot_lang.ast.expression.FunCall):
            return [], f"{expression.function}({', '.join([self.skillset] + list(map(self.__build_pre_expr, expression.parameters)))})"

        else:
            raise NotRecognized(expression)

    def generate(self, output):
        operators = []
        self.skillset = self.model.name.capitalize()
        self._logger.info(f"skillset {self.skillset}")

        for skill in self.model.skills:

            params = []
            preconds = []
            preconds_resources = set()
            preconds_predicates = set()
            addlist = []
            dellist = []
            effects = []

            for i in skill.inputs:
                params.append(i.name.capitalize())

            for pre in skill.preconditions:
                if pre.expression:
                    preconds_predicates = functions_in_expression(pre.expression)
                    f = self.__build_pre_expr(pre.expression)
                    if isinstance(f, str): f = [f]
                    preconds.append(f)

                if pre.resource:
                    preconds_resources |= resources_in_expression(pre.resource)
                    f = self.__build_resource_pre(pre.resource)
                    if isinstance(f, str): f = [f]
                    preconds.append(f)

            if skill.preconditions.success:
                effects.append(skill.effects[skill.preconditions.success])

            mode = skill.modes.list[0]
            self._logger.info(f'considered nominal mode for skill {skill.name}: {mode}')

            if mode.expression:
                dels, adds = self.__build_post_expr(mode.expression)
                if isinstance(adds, str): adds = [adds]
                if isinstance(dels, str): dels = [dels]
                addlist.append(adds)
                dellist.append(dels)

                #preds = functions_in_expression(mode.expression) - preconds_predicates
                #self._logger.debug(preds)
                #for p in preds:
                #    fun = self.model.functions[p]
                #    prop = [f"{fun.name.lower()}({','.join([p.name.capitalize() for p in fun.parameters])})"]
                #    dellist.append(prop)
                #    preconds.append(prop)

            if mode.effect:
                effects.append(skill.effects[mode.effect])
            for eff in effects:
                for e in eff.effects:
                    addlist.append([f"{e.resource.lower()}({self.skillset}, {e.state.lower()})"])
                    if e.resource in preconds_resources:
                        self._logger.debug(f"resource {e.resource} in preconditions")
                        self._logger.debug(preconds)
                        pre_res = next(p for p in flatten(preconds) if e.resource.lower() in p)
                        dellist.append([pre_res])
                    else:
                        self._logger.debug(f"resource {e.resource} NOT in preconditions")
                        free_del = f"{e.resource.lower()}({self.skillset}, {e.resource.capitalize()}_state)"
                        preconds_resources.add(e.resource)
                        preconds.append([free_del])
                        dellist.append([free_del])
                    
            addlist.append([f"{skill.name.lower()}_{mode.name.lower()}({', '.join([self.skillset] + params)})"])

            self._logger.debug(f"preconds: {[p for p in flatten(preconds) if p]}")
            self._logger.debug(f"dellist: {[d for d in flatten(dellist) if d]}")
            self._logger.debug(f"addlist: {[a for a in flatten(addlist) if a]}")

            name = f"{skill.name.lower()}({', '.join([self.skillset] + params)})"
            opn = f"""opn( {name},
     [{', '.join([p for p in flatten(preconds) if p])}],
     [{', '.join([d for d in flatten(dellist) if d])}],
     [{', '.join([a for a in flatten(addlist) if a])}] )."""
            
            self._logger.info(opn)
            operators.append(opn)

        self._logger.debug("Generating file %s", output)
        f = open(output, 'w')
        for opn in operators:
            f.write(opn)
            f.write('\n\n')
        f.close()

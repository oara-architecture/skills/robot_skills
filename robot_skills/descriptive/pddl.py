import functools
import operator
import robot_lang

from ..common.generator import Generator
from ..common.templates import resources_in_expression, functions_in_expression
from ..common.exceptions import NotSupported, NotRecognized

def flatten(l):
    return functools.reduce(operator.iconcat, l, [])

class PDDLGenerator(Generator):
    def __init__(self, model: robot_lang.Model, types: robot_lang.TypeBlock, with_skillset: bool = False):
        super().__init__(model, 'pddl')
        self.__types = types
        self.__with_skillset = with_skillset

    def __build_resource_pre(self, expression):
        if isinstance(expression, robot_lang.ast.resource_expression.BinBoolRes):
            if expression.op == '&&':
                left = self.__build_resource_pre(expression.left)
                right = self.__build_resource_pre(expression.right)
                return left + right
            elif expression.op == '||':
                raise NotSupported(expression)
            elif expression.op == '=>':
                raise NotSupported(expression)
            else:
                raise NotRecognized(expression)

        elif isinstance(expression, robot_lang.ast.resource_expression.BinCompRes):
            if expression.op == '==':
                if self.__with_skillset:
                    return [f"({expression.resource.lower()} ?{self.model.name} {expression.resource.upper()}_{expression.state.upper()})"]
                else:
                    return [f"({expression.resource.lower()} {expression.resource.upper()}_{expression.state.upper()})"]
            elif expression.op == '!=':
                if self.__with_skillset:
                    return [f"(not ({expression.resource.lower()} ?{self.skillset} {expression.resource.upper()}_{expression.state.upper()}))"]
                else:
                    return [f"(not ({expression.resource.lower()} {expression.resource.upper()}_{expression.state.upper()}))"]
            else:
                raise NotRecognized(expression)

        else:
            raise NotRecognized(expression)

    def __build_pre_expr(self, expression):
        if isinstance(expression, robot_lang.ast.expression.BinBool):
            if expression.op == '&&':
                left = self.__build_pre_expr(expression.left)
                right = self.__build_pre_expr(expression.right)
                return [left, right]
            elif expression.op == '||':
                raise NotSupported(expression)
            elif expression.op == '=>':
                raise NotSupported(expression)
            else:
                raise NotRecognized(expression)

        elif isinstance(expression, robot_lang.ast.expression.UnaryBool):
            if expression.op == '!':
                return self.__build_pre_expr(expression.expr)
            else:
                raise NotRecognized(expression)

        elif isinstance(expression, robot_lang.ast.expression.BinComp):
            left = self.__build_pre_expr(expression.left)
            
            if isinstance(expression.right, robot_lang.ast.expression.BoolValue):
                if expression.right.value:
                    if expression.op == '==':
                        return left
                    elif expression.op == '!=':
                        return f"(not {left})"
                else:
                    if expression.op == '==':
                        return f"(not {left})"
                    elif expression.op == '!=':
                        return left

            else:
                right = self.__build_pre_expr(expression.right)
                if expression.op == '==':
                    return f"(= {left} {right})"
                elif expression.op == '!=':
                    return f"(not (= {left} {right}))"
                else:
                    raise NotRecognized(expression)

        elif isinstance(expression, robot_lang.ast.expression.BoolValue):
            return f"{expression.value}"

        elif isinstance(expression, robot_lang.ast.expression.Number):
            return f"{expression.value}"

        elif isinstance(expression, robot_lang.ast.expression.Identifier):
            return f"?{expression.name}"

        elif isinstance(expression, robot_lang.ast.expression.FunCall):
            if self.__with_skillset:
                params = [self.skillset] + list(map(self.__build_pre_expr, expression.parameters))
            else:
                params = list(map(self.__build_pre_expr, expression.parameters))
            return f"({expression.function} {' '.join(params)})"

        else:
            raise NotRecognized(expression)

    def __build_post_expr(self, expression):
        if isinstance(expression, robot_lang.ast.expression.BinBool):
            if expression.op == '&&':
                delleft, addleft = self.__build_post_expr(expression.left)
                delright, addright = self.__build_post_expr(expression.right)
                return [delleft, delright], [addleft, addright]
            else:
                raise NotSupported(expression)

        elif isinstance(expression, robot_lang.ast.expression.UnaryBool):
            if expression.op == '!':
                dels, adds = self.__build_post_expr(expression.expr)
                return adds, dels
            else:
                raise NotSupported(expression)

        elif isinstance(expression, robot_lang.ast.expression.BinComp):
            delleft, addleft = self.__build_post_expr(expression.left)
            
            if isinstance(expression.right, robot_lang.ast.expression.BoolValue):
                if expression.right.value:
                    if expression.op == '==':
                        return delleft, addleft
                    elif expression.op == '!=':
                        return addleft, delleft
                else:
                    if expression.op == '==':
                        return addleft, delleft
                    elif expression.op == '!=':
                        return delleft, addleft

            else:
                delright, addright = self.__build_post_expr(expression.right)
                if expression.op == '==':
                    return [], f"(= {addleft} {addright})"
                elif expression.op == '!=':
                    return [], f"(not (= {addleft} {addright}))"
                else:
                    raise NotRecognized(expression)

        elif isinstance(expression, robot_lang.ast.expression.BoolValue):
            return [], f"{expression.value}"

        elif isinstance(expression, robot_lang.ast.expression.Number):
            return [], f"{expression.value}"

        elif isinstance(expression, robot_lang.ast.expression.Identifier):
            return [], f"{expression.name.capitalize()}"

        elif isinstance(expression, robot_lang.ast.expression.FunCall):
            if self.__with_skillset:
                params = [self.skillset] + list(map(self.__build_pre_expr, expression.parameters))
            else:
                params = list(map(self.__build_pre_expr, expression.parameters))
            return [], f"({expression.function} {' '.join(params)})"

        else:
            raise NotRecognized(expression)

    def generate(self, output):
        self.skillset = self.model.name.capitalize()
        self._logger.info(f"skillset {self.skillset}")

        self._logger.debug("Generating file %s", output)
        f = open(output, 'w')

        f.write(f"(define (domain {self.model.name})\n")
        f.write(f"\t(:requirements :strips :typing)\n")

        types = [t.name for t in self.__types]
        if self.__with_skillset:
            types.append(self.skillset)
        for res in self.model.resources:
            types.append(f"{res.name.capitalize()}_State")
        f.write(f"\t(:types {' '.join(types)})\n")

        f.write(f"\t(:constants\n")
        for res in self.model.resources:
            states = {f"{res.name.upper()}_{res.statemachine.initial.upper()}"}
            states |= {f"{res.name.upper()}_{a.src.upper()}" for a in res.statemachine.arcs}
            states |= {f"{res.name.upper()}_{a.dst.upper()}" for a in res.statemachine.arcs}
            f.write(f"\t\t{' '.join(states)} - {res.name.capitalize()}_State\n")
        f.write(f"\t)\n") #:constants

        f.write(f"\t(:predicates\n")
        for fun in self.model.functions:
            if self.__with_skillset:
                params = [robot_lang.ast.function.Parameter(None, self.model.name, self.skillset)] + fun.parameters
            else:
                params = fun.parameters
            f.write(f"\t\t({fun.name} {' '.join(f'?{p.name} - {p.type}' for p in params)})\n")
        for res in self.model.resources:
            if self.__with_skillset:
                f.write(f"\t\t({res.name} ?{self.model.name} - {self.skillset} ?state - {res.name.capitalize()}_State)\n")
            else:
                f.write(f"\t\t({res.name} ?state - {res.name.capitalize()}_State)\n")
        for skill in self.model.skills:
            result = skill.results.list[0]
            self._logger.info(f'considered nominal result for skill {skill.name}: {result}')
            if self.__with_skillset:
                params = [robot_lang.ast.input.Input(None, self.model.name, self.skillset)] + list(skill.inputs)
            else:
                params = list(skill.inputs)
                f.write(f"\t\t({skill.name}_{result.name.lower()} {' '.join(f'?{p.name} - {p.type}' for p in params)})\n")
        f.write(f"\t)\n") # (:predicates)

        for skill in self.model.skills:
            f.write(f"\t(:action {skill.name}\n")
            
            if self.__with_skillset:
                params = [robot_lang.ast.input.Input(None, self.model.name, self.skillset)] + list(skill.inputs)
            else:
                params = list(skill.inputs)

            preconds = []
            preconds_resources = set()
            preconds_predicates = set()
            addlist = []
            dellist = []
            effects = []

            for pre in skill.preconditions:
                if pre.expression:
                    preconds_predicates = functions_in_expression(pre.expression)
                    form = self.__build_pre_expr(pre.expression)
                    if isinstance(form, str): form = [form]
                    preconds += form

                if pre.resource:
                    preconds_resources |= resources_in_expression(pre.resource)
                    form = self.__build_resource_pre(pre.resource)
                    if isinstance(form, str): form = [form]
                    preconds += form

            if skill.preconditions.success:
                effects.append(skill.effects[skill.preconditions.success])

            result = skill.results.list[0]
            self._logger.info(f'considered nominal result for skill {skill.name}: {result}')

            if result.expression:
                dels, adds = self.__build_post_expr(result.expression)
                if isinstance(adds, str): adds = [adds]
                if isinstance(dels, str): dels = [dels]
                addlist.append(adds)
                dellist.append(dels)

            if result.effect:
                effects.append(skill.effects[result.effect])
            for eff in effects:
                for e in eff.effects:
                    if self.__with_skillset:
                        result = f"({e.resource.lower()} {self.model.name} {e.resource.upper()}_{e.state.upper()})"
                    else:
                        result = f"({e.resource.lower()} {e.resource.upper()}_{e.state.upper()})"
                    addlist.append([result])
                    if e.resource in preconds_resources:
                        self._logger.debug(f"resource {e.resource} in preconditions")
                        self._logger.debug(preconds)
                        pre_res = next(p for p in preconds if e.resource.lower() in p)
                        dellist.append([pre_res])
                    else:
                        self._logger.debug(f"resource {e.resource} NOT in preconditions")
                        if self.__with_skillset:
                            free_del = f"({e.resource.lower()} ?{self.model.name} ?{e.resource}_state)"
                        else:
                            free_del = f"({e.resource.lower()} ?{e.resource}_state)"
                        preconds_resources.add(e.resource)
                        preconds += [free_del]
                        dellist.append([free_del])

            if self.__with_skillset:                  
                result = f"({skill.name.lower()}_{result.name.lower()} ?{self.model.name} {' '.join(f'?{p.name}' for p in params)})"
            else:
                result = f"({skill.name.lower()}_{result.name.lower()} {' '.join(f'?{p.name}' for p in params)})"
            addlist.append([result])

            for p in preconds_resources:
                params.append(robot_lang.ast.input.Input(None, f"{p}_state", f"{p.capitalize()}_State"))
            for d in self.model.data:
                params.append(d)
            if params:
                f.write(f"\t\t:parameters ({' '.join(f'?{p.name} - {p.type}' for p in params)})\n")

            self._logger.debug(f"preconds: {[p for p in preconds if p]}")
            self._logger.debug(f"dellist: {[d for d in flatten(dellist) if d]}")
            self._logger.debug(f"addlist: {[a for a in flatten(addlist) if a]}")

            f.write("\t\t:precondition (and\n")
            for pre in preconds:
                f.write(f"\t\t\t{pre}\n")
            f.write("\t\t)\n") #(:precondition)

            f.write("\t\t:effect (and\n")
            for e in flatten(addlist):
                if e: f.write(f"\t\t\t{e}\n")
            for e in flatten(dellist):
                if e: f.write(f"\t\t\t(not {e})\n")
            f.write("\t\t)\n") #(:effect)

            f.write(f"\t)\n") # (:action

        f.write(")") #(define
        f.close()

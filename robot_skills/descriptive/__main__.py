import sys
import argparse
import json
import yaml
import traceback

import robot_lang

from ..common.model import get_model
from ..common.logger import logging, setup_from_arg
from .strips_logic import StripsLogicGenerator
from .pddl import PDDLGenerator

log = logging.getLogger('robot_skills.descriptive')


def main():
    parser = argparse.ArgumentParser(
        prog='python3 -m robot_skills.descriptive', description='Generate descriptive models')
    parser.add_argument('model', type=str, nargs='+', help='model files')
    parser.add_argument('-s', '--skillset', 
        help='Skillset name. Must be specified if the input model contains several skillsets.')
    parser.add_argument('-l', '--log', 
        help='Log level (default: info)', default='info')
    parser.add_argument('-o', '--output', type=str,
        help='Output file.')

    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument("--pddl", action='store_true',
                       help="Generate a PDDL domain")
    #group.add_argument("--prolog", action='store_true',
    #                   help="Generate a Prolog STRIPS model")

    args = parser.parse_args()
    setup_from_arg(args.log)

    try:
        model = robot_lang.parse_all(args.model)
    except FileNotFoundError:
        log.error("Model file %s not found", args.model)
        sys.exit(1)

    try:
        skillset = get_model(model, args.skillset)

        if args.pddl:
            log.info("Generating PDDL model")
            gen = PDDLGenerator(skillset, types=model.types)
            if args.output is None:
                args.output = f"{skillset.name}.pddl"
        #elif args.prolog:
        #    log.info("Generating a Logic STRIPS model")
        #    gen = StripsLogicGenerator(skillset)
        #    if args.output is None:
        #        args.output = f"{skillset.name}.pl"

        gen.generate(args.output)

    except StopIteration:
        log.error("The model does not contain any skillset: nothing is generated")
        traceback.print_exc()
        sys.exit(1)

    except Exception:
        log.error("Cannot generate from this model")
        traceback.print_exc()
        sys.exit(1)


if __name__ == '__main__':
    main()

# Formalization of Robot Skills with Descriptive and Operational Models

The documentation of the Robot Skills language and tools is available at

<center>
 <a href="https://oara-architecture.gitlab.io/robot-skills">https://oara-architecture.gitlab.io/robot-skills</a>
</center>

#!/usr/bin/env python3

from distutils.core import setup

setup(name='robot_skills',
      version='1.0',
      description='Generator of operational and descriptive models from the robot_lang DSL',
      author='Charles Lesire',
      author_email='charles.lesire@onera.fr',
      url='https://gitlab.com/oara-architecture/robot_skills',
      packages=[
            'robot_skills', 
            'robot_skills.common',
            'robot_skills.managers',
            'robot_skills.interface',
            'robot_skills.descriptive'],
      package_data={'robot_skills': [
          'templates/ros/*', 'templates/common/*', 'templates/ros2/*', 'templates/bridge/*']},
)
